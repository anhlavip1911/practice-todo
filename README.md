:arrow_up: How to Setup

Step 1: git clone this repository: git clone [repo_url]

Step 2: cd to the cloned repository: cd app-web

Step 3: Install the Application with npm

:arrow_forward: How to Run App

cd to the repo

Run & Build

npm start
 
View app on http://localhost:3000/

## Technology used
- Html5, Css3, Js
- ReactJs, Redux, React-router-dom
- React-bootstrap
- Firebase, Firestore
- Moment js
- Materialize css

## Estimate project

|       Title         |Intend time|
|----------------|-------------------------------|
|Analysis project|4h            
|Build codebase    |4h          
|Sign In, Sign Up, Profile page UI|8h
|Todo list home page UI |8h
|Sign In, Sign Up, Logout features |6h
|Update Profile features | 4h
|CRUD features | 16h
|Optimal conditions|16h
|Fixbugs | 32h 

Total estimated time: 98h

