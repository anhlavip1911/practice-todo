import firebase from "firebase/app"
import "firebase/auth"

const app = firebase.initializeApp({
  apiKey: "AIzaSyD206tUJc_twzBJm1WsNJtqvbDWVyZpJMc",
  authDomain: "todoapp-91591.firebaseapp.com",
  projectId: "todoapp-91591",
  storageBucket: "todoapp-91591.appspot.com",
  messagingSenderId: "292085395321",
  appId: "1:292085395321:web:25b68ec2ce24ef71ca09a8",
  measurementId: "G-BS5ZS369LG"
})

export const auth = app.auth()
export default app