import React from 'react'
import { Container } from 'react-bootstrap';
import AuthProvider from './contexts/AuthContext';
import Rou from './router/Router'
// import '../index.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

export default function App() {
    return (
        <Container
            className="d-flex align-items-center justify-content-center"
            style={{ minHeight: '100vh' }}
        >
            <div className="w-100" style={{ maxWidth: "400px" }}>
                <Router>
                    <AuthProvider>
                        <Rou />
                    </AuthProvider>
                </Router>
            </div>
        </Container>
    )
}
