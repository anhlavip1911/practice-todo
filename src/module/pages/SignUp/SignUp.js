import React, { useRef, useState } from 'react'
import { Form, Button, Card, Alert } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../../contexts/AuthContext';

export default function SingUp() {

    const emailRef = useRef();
    const passwordRef = useRef();
    const passwordConfirmRef = useRef();
    const { singup, currentUser } = useAuth();
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const history = useHistory()

    async function handleSubmit(e) {
        e.preventDefault()
        if (passwordRef.current.value !== passwordConfirmRef.current.value) {
            return setError('Password do not match')
        }
        try {
            setError('Sign up success');
            setLoading(true)
            await singup(emailRef.current.value, passwordRef.current.value)
            history.push("/login")
        } catch (error) {
            setError(error.message)
        }
        setLoading(false)
    }

    return (
        <>
            <div className="container">
                {error && <Alert variant="danger">{error}</Alert>}
                <form className="white" onSubmit={handleSubmit}>
                    <h2 className="text-center mb-4">Sign Up</h2>
                    <div className="input-field">
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" id='firstName' />
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" id='lastName' />
                    </div>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type='email' ref={emailRef} id='email' />
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input type='password' ref={passwordRef} id='password' />
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password Confirmation</label>
                        <input type='password' ref={passwordConfirmRef} id='password' />
                    </div>
                    <div className="input-field">
                        <Button disabled={loading} className="w-100" type="submit">Sign Up</Button>
                    </div>
                </form>
                <h4 className="w-100 text-center mt-2">
                    Already have an account? <Link to="/login">Log In</Link>
                </h4>
            </div>
        </>
    )
}
