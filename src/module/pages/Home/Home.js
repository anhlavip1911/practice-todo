import React, { useContext, useState } from 'react'
import { Button, Card, Alert } from 'react-bootstrap'
import { useAuth } from "../../contexts/AuthContext"
import { Link, useHistory } from 'react-router-dom';

export default function Home() {

    const history = useHistory()
    const [error, setError] = useState("")
    const { currentUser, logout } = useAuth()
    async function handleLogout() {
        try {
            await logout()
            history.push("/login")
        } catch (error) {
            setError("Failed to log out")
        }
    }

    return (
        <>
            <Card>
                <Card.Body>
                    <h2 className="text-center mb-4">Profile</h2>
                    {error && <Alert variant="danger">{error}</Alert>}
                    <strong>Email: </strong> {currentUser.email}
                    <Link to="/update-profile" className="btn btn-primary w-100 mt-3">Update Profile</Link>
                </Card.Body>
            </Card>
            <div className="w-100 text-center mt-2">
                <Button variant="Link" onClick={handleLogout}>Log Out</Button>
            </div>
        </>
    )
}
