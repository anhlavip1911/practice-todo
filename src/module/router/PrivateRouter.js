import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'

export default function PrivateRouter({component: Component, ...rest}) {

    const { currentUser } = useAuth();

    return (
        <Route {...rest} render={(props) => {
            return currentUser ? <Component {...props} /> : <Redirect to='/login' />
        }} />
    )
}

