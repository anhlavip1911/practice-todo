import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from '../pages/Home/Home'
import SignIn from '../pages/SignIn/SignIn'
import SingUp from '../pages/SignUp/SignUp'
import PrivateRouter from './PrivateRouter'
import UpdateProfile from '../pages/UpdateProfile/UpdateProfile'

export default function Rou() {
    return (
        <div>
            <Switch>
            <PrivateRouter exact path="/" component={Home}/>
            <PrivateRouter path="/update-profile" component={UpdateProfile}/>
                <Route path="/signup" component={SingUp}/>
                <Route path="/login" component={SignIn}/>
            </Switch>
        </div>
    )
}
